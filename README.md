LD_TRICK
- This program generates a random int value from rand() function, you have to guess that or you can be little cheeky.
- Make a shared library with rand(), which returns the value you want.
- Compilation: gcc -shared -fPIC example.c -o example.so
- Run: LD_PRELOAD=$PWD/example.so ./random_num